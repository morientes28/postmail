export TEMPLATE_SK=`cat template_sk.txt`
export TEMPLATE_EN=`cat template_en.txt`

send(){

	TEMP=${TEMPLATE_SK//USER_EMAIL/$name}
	TEMP2=${TEMP//USER_PASSWORD/$password}'<br><hr>'

	TEMP3=${TEMPLATE_EN//USER_EMAIL/$name}
	TEMP4=${TEMP3//USER_PASSWORD/$password}
	SEND_BODY=$TEMP2$TEMP4
	echo $SEND_BODY
	curl "https://api.postmarkapp.com/email" \
	  -X POST \
	  -H "Accept: application/json" \
	 -H "Content-Type: application/json" \
	  -H "X-Postmark-Server-Token: e74d0245-a7e5-4db0-af95-e70d9477edfa" \
	  -d "{From: 'intranet@bbx.sk', To: '$name', Subject: 'Access to DTShS', HtmlBody: '$SEND_BODY'}"
	return 1
}


count=0
name=""
password=""
while IFS='' read -r line || [[ -n "$line" ]]; do
    if (( $count % 2 == 1 ))
	then
		 password=$line
	     body=$(send)
	     echo $body  
	else
		name="$line"
	fi
    ((count++))
done < "password-test.sql"
echo $count

